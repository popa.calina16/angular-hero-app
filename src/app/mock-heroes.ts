import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Wonderwoman' },
  { id: 12, name: 'Green Lantern' },
  { id: 13, name: 'Robyn' },
  { id: 14, name: 'Superman' },
  { id: 15, name: 'Aquaman' },
  { id: 16, name: 'Cyclops' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'The Flash' },
  { id: 19, name: 'Batman' },
  { id: 20, name: 'Shazam' }
];
